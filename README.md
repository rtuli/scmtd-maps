# SCMTD Maps

Includes:
*  Coverage map of Santa Cruz METRO's service area in compliance with HSC 50093.5. 
*  GIS analysis used for Route 3 redesign
*  Frequency based system map
*  Stop data in csv and geojson format

QGIS 3.10 is needed to view most of these maps.